/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.sourcemanager.service;

import org.kathra.utils.ApiResponse;
import javax.activation.FileDataSource;
import org.kathra.sourcemanager.model.Folder;
import java.util.List;
import org.kathra.core.model.Membership;
import org.kathra.core.model.SourceRepository;
import org.kathra.core.model.SourceRepositoryCommit;

public interface SourceManagerService extends org.kathra.iface.KathraAuthRequestHandler {

    /**
    * Add multiple memberships in specified projects
    * 
    * @param memberships  (required)
    * @return ApiResponse
    */
    ApiResponse addMemberships(List<Membership> memberships) throws Exception;

    /**
    * Create a new branch in an existing Kathra SourceRepository
    * 
    * @param sourceRepositoryPath SourceRepository's Path (required)
    * @param branch  (required)
    * @param branchRef  (optional)
    * @return String
    */
    String createBranch(String sourceRepositoryPath, String branch, String branchRef) throws Exception;

    /**
    * Create new commit in branch
    * 
    * @param sourceRepositoryPath SourceRepository's Path (required)
    * @param branch SourceRepository's branch (required)
    * @param file File to commit (required)
    * @param filepath The location in which the file has to be commited (optional)
    * @param uncompress Boolean to indicate if provided file should be uncompressed before being commited (optional, default to false)
    * @param tag Git tag to put on the commit (optional)
    * @param replaceRepositoryContent Boolean to indicate if provided file should replace the whole repository content (optional, default to false)
    * @return SourceRepositoryCommit
    */
    SourceRepositoryCommit createCommit(String sourceRepositoryPath, String branch, FileDataSource file, String filepath, Boolean uncompress, String tag, Boolean replaceRepositoryContent) throws Exception;

    /**
    * Create new deployKey in specified project
    * 
    * @param keyName  (required)
    * @param sshPublicKey  (required)
    * @param sourceRepositoryPath SourceRepository's Path (required)
    * @return ApiResponse
    */
    ApiResponse createDeployKey(String keyName, String sshPublicKey, String sourceRepositoryPath) throws Exception;

    /**
    * Create a new folder in the Source Repository Provider
    * 
    * @param folder Folder object (required)
    * @return Folder
    */
    Folder createFolder(Folder folder) throws Exception;

    /**
    * Create a new Source Repository in Kathra Repository Provider
    * 
    * @param sourceRepository SourceRepository object to be created (required)
    * @param deployKeys A list of deployKey Ids to enable in the created source repository (optional)
    * @return SourceRepository
    */
    SourceRepository createSourceRepository(SourceRepository sourceRepository, List<String> deployKeys) throws Exception;

    /**
    * Delete multiple memberships in specified projects
    * 
    * @param memberships  (required)
    * @return ApiResponse
    */
    ApiResponse deleteMemberships(List<Membership> memberships) throws Exception;

    /**
    * Delete Source Repository in Kathra Repository Provider
    * 
    * @param sourceRepositoryPath SourceRepository's Path (required)
    * @return String
    */
    String deleteSourceRepository(String sourceRepositoryPath) throws Exception;

    /**
    * Retrieve accessible branches in an existing Kathra SourceRepository
    * 
    * @param sourceRepositoryPath SourceRepository's Path (required)
    * @return List<String>
    */
    List<String> getBranches(String sourceRepositoryPath) throws Exception;

    /**
    * Retrieve accessible commits in an existing branch
    * 
    * @param sourceRepositoryPath SourceRepository's Path (required)
    * @param branch SourceRepository's branch (required)
    * @return List<SourceRepositoryCommit>
    */
    List<SourceRepositoryCommit> getCommits(String sourceRepositoryPath, String branch) throws Exception;

    /**
    * Retrieve an existing file in an existing branch
    * 
    * @param sourceRepositoryPath SourceRepository's Path (required)
    * @param branch SourceRepository's branch (required)
    * @param filepath The location of the file to retrieve (required)
    * @return FileDataSource
    */
    FileDataSource getFile(String sourceRepositoryPath, String branch, String filepath) throws Exception;

    /**
    * Retrieve specified folder object
    * 
    * @param folderPath Folder's ID in which artifacts will be created (required)
    * @return Folder
    */
    Folder getFolder(String folderPath) throws Exception;

    /**
    * Retrieve accessible folders for user using provided identity
    * 
    * @return List<Folder>
    */
    List<Folder> getFolders() throws Exception;

    /**
    * Retrieve memberships in specified project
    * 
    * @param sourceRepositoryPath SourceRepository's Path (required)
    * @param memberType Type of memberships to retrieve, return all types if not specified (optional)
    * @return List<Membership>
    */
    List<Membership> getMemberships(String sourceRepositoryPath, String memberType) throws Exception;

    /**
    * Retrieve accessible Source Repositories in the specified folder
    * 
    * @param folderPath Folder's ID in which artifacts will be created (required)
    * @return List<SourceRepository>
    */
    List<SourceRepository> getSourceRepositoriesInFolder(String folderPath) throws Exception;

}
